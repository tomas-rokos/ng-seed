var webpack = require('webpack');
var helpers = require('../helpers');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devtool: 'inline-source-map',

    resolve: {
        extensions: [ '.ts', '.js' ]
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: ['istanbul-instrumenter-loader', 'awesome-typescript-loader', 'angular2-template-loader'],
                exclude: /\.spec\.ts$/
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.less$/,
                include: helpers.root('src'),
                loaders: [
                    'to-string-loader',
                    'css-loader',
                    'less-loader',
                ]
            },
        ]
    },

    plugins: [
        // https://github.com/angular/angular/issues/11580
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /\@angular(\\|\/)core(\\|\/)esm5/,
            helpers.root('./src') // location of your src
        ),   
    ]
};