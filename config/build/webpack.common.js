const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const helpers = require('../helpers');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': './src/main.ts',
    },

    resolve: {
        extensions: [
            '.js', '.ts'
        ],
        modules: [
            helpers.root('./'),
            helpers.root('src'),
            helpers.root('node_modules'),
        ]
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: [
                    'awesome-typescript-loader',
                    'angular-router-loader',
                    'angular2-template-loader'
                ]
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|otf|ttf|eot|ico)$/,
                use: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.css$/,
                include: [ 
                    helpers.root('src'), 
                    helpers.root('node_modules') 
                ],
                use: [
                    { loader: 'to-string-loader' },
                    { 
                        loader: 'css-loader',
                        options: {
                            paths: [
                                helpers.root('node_modules'),
                                helpers.root('src'),
                            ]
                        }
                    }
                ]
            },
            {
                test: /\.less$/,
                include: [ 
                    helpers.root('./'),
                    helpers.root('src'), 
                    helpers.root('node_modules'),
                ],
                use: [
                    { loader: 'to-string-loader' },
                    { loader: 'css-loader' },
                    {
                        loader: 'less-loader',
                        options: {
                            paths: [
                                helpers.root('./'),
                                helpers.root('node_modules'),
                                helpers.root('src'),
                            ]
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: [ 'app', 'vendor', 'polyfills' ]
        }),

        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        
        new CopyWebpackPlugin([{
            from: 'src/assets',
            to: './assets'
        }]),

        // https://github.com/angular/angular/issues/11580
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /\@angular(\\|\/)core(\\|\/)esm5/,
            helpers.root('./src') // location of your src
        ),
    ]
};