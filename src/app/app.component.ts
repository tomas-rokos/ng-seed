import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'the-app',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.less' ]
})
export class AppComponent implements OnInit {

    public constructor() {}

    public ngOnInit(): void {
    }
}
