## Angular 5 Seed Project

* `npm start` - start the development webpack server (access via http://localhost:3000/)
* `npm test` - run the project unit tests (*.spec.ts files)
* `npm run coverage` - run the project unit tests one time and print out a coverage report, generated under **/coverage/index.html**
* `npm run build:developmet` - generate a development build for the project, which will be inserted into dist/ (in code process.env.NODE_ENV = process.env.ENV = 'development')
* `npm run build:beta` - generate a beta build for the project, which will be inserted into dist/ (in code process.env.NODE_ENV = process.env.ENV = 'beta')
* `npm run build:production` - generate a beta build for the project, which will be inserted into dist/ (in code process.env.NODE_ENV = process.env.ENV = 'production')
